#!/usr/bin/env python3

import sys
try:
    import pyperclip
except ImportError:
    print("Missing requirement: pyperclip (install via pip)")

zeroWidth = '\u200b'
callSign = "hT001:"
usage = """"Usage:

`hT001.py encode [publicText] [secretText]` (encoded text will be copied to the clipboard)
`ht001.py decode` (clipboard will be decoded)"""

def encode(publicText, secretText):
    if len(publicText) < len(secretText)+len(callSign)+1:
        raise ValueError("Public Text must be at least {0} characters longer than secret text.".format(len(callSign)+1))
    toEncode = callSign + secretText
    outputText = ""
    i = 0
    while i < len(toEncode):
        outputText += publicText[i]
        for i2 in range(ord(toEncode[i])+1):
            outputText += zeroWidth
        i += 1
    while i < len(publicText):
        outputText += publicText[i]
        i += 1
    return outputText

def decode(encodedText):
    decodedValues = []
    value = 0
    for i in encodedText:
        if i == zeroWidth:
            value += 1
        else:
            if value > 0:
                decodedValues.append(value-1)
            value = 0
    decodedText = ""
    for i in decodedValues:
        decodedText += chr(i)
        if not (callSign.startswith(decodedText) or decodedText.startswith(callSign) or decodedText == ""):
            print(decodedValues)
            raise ValueError("Unsupported call sign/encoding type: `" + decodedText + "`")
    outputText = decodedText[len(callSign):]
    return outputText

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(usage)
        exit(1)
    if not sys.argv[1] in ["encode","decode"]:
        print(usage)
        exit(1)
    if sys.argv[1] == "encode":
        try:
            pyperclip.copy(encode(sys.argv[2],sys.argv[3]))
            print("Encoded text copied to the clipboard!")
        except ValueError as e:
            print("Error: " + str(e))
            exit(1)
        except IndexError:
            print(usage)
            exit(1)
    if sys.argv[1] == "decode":
        try:
            print(decode(pyperclip.paste()))
        except ValueError as e:
            print("Error: " + str(e))
            exit(1)
        except IndexError:
            print(usage)
            exit(1)
